﻿using CheckPalindrome;
using System;

namespace lv7
{
    class Program
    {
        static void Main(string[] args)
        {
            PalindromeChecker palindromeChecker = new PalindromeChecker();
            Console.WriteLine("Check if your message is palindrome: ");

            try
            {
                string message = Console.ReadLine();
                if (palindromeChecker.IsPalindrome(message))
                {
                    Console.WriteLine("Your message is palindrome");
                }

                else
                {
                    Console.WriteLine("Your message is not palindrome");
                }

            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }

           
            

        }
    }
}
