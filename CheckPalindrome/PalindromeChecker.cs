﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CheckPalindrome
{
   public class PalindromeChecker

    {
        public bool IsPalindrome(string message)
        {
           if(string.IsNullOrEmpty(message) || string.IsNullOrWhiteSpace(message))
            {
                throw new ArgumentException("Invalid input");
            }

            string upperMessage = message.ToUpper();
            List<char> separatedMessage = new List<char>(upperMessage.ToCharArray());

            for (int i = 0; i < separatedMessage.Count; i++)
            {
                if(separatedMessage[i] >= ' ' && separatedMessage[i] < '0' || separatedMessage[i] > '9' && separatedMessage[i] < 'A' || separatedMessage[i] > 'Z' && separatedMessage[i] < 'a' || separatedMessage[i] > 'z')
                {
                    separatedMessage.RemoveAt(i);
                    i--;
                }
            }
            upperMessage = new string(separatedMessage.ToArray());
            return upperMessage.SequenceEqual(upperMessage.Reverse());




        }
    }
}
