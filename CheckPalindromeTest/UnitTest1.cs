﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using CheckPalindrome;

namespace CheckPalindromeTest
{



    [TestFixture]
    public class UnitTest1
    {
        PalindromeChecker palindromeChecker = new PalindromeChecker();
        


        [TestCase("ANA")]
        [TestCase("Ana")]
        [TestCase("Ana voli Milovana")]
        [TestCase("Ana voli Milovana.")]
        [TestCase("Ana voli Milovana.")]
   


        public void IsPalindrome_WhenInputIsPalindrome_ReturnsTrue(string input)
        {
           
            bool expected = true;
            bool actual = palindromeChecker.IsPalindrome(input);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expected, actual);
        }



        [TestCase("")]
        [TestCase("   ")]
        


        public void IsPalindrome_WhenInputIsEmpty_ThrowsArgumentException(string input)
        {
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.ThrowsException<ArgumentException>(() => palindromeChecker.IsPalindrome(input));
        }


        [TestCase("Ana banana")]
        [TestCase("Ana-Marija hoda.")]
        public void IsPalindrome_WhenInputIsNotPalindrome_ReturnsFalse(string input)
        {
            bool expected = false;
            bool actual = palindromeChecker.IsPalindrome(input);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expected, actual);
        }
    }
}
